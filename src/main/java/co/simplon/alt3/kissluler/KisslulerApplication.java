package co.simplon.alt3.kissluler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KisslulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KisslulerApplication.class, args);
	}

}
